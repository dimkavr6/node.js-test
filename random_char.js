var randomstring = require('randomstring');

var getRandomChar = function(){
  var randString = randomstring.generate({
    length: 100,
    charset: 'alphabetic'
  });
  return randString;
};

module.exports = getRandomChar;
