//экспортирую нужные мне функции из других файлов в проекте
var rand_string = require('./random_char');
var rand_int = require('./random_int');

//подрубаю express
var express = require('express');
var app = express();

app.get('/:name',function(req, res) {
  if(req.params.name == 'get_string'){
    res.send(rand_string());
  }
  else if(req.params.name == 'get_int'){
    res.send(rand_int(0,200));
  } else {
    res.send('please retry later');
  }

});

app.listen(8088);
